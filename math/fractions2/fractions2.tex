%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 

% Notes on Graphics
% Use Inkscape, set point size of fonts in Inkscape to match the
% LaTeX point size (e.g., [11pt]{article} set fonts to 11 pt), but
% then scale \includgraphics[scale=1.25]{file.pdf} the file on import.
% Since everything is scaled, it is helpful to understand Inkscape's
% units. Internally, Inkscape uses 90 dpi. The default 11pt twoside,
% letterpaper option for LaTeX article class has 360pt textwidth,
% which translates into 4.981in. 4.981/1.25=3.985in maximum width in
% Inkscape, or if you prefer pixel units, 358.655 pixels. 

\documentclass[letterpaper,11pt,twoside]{article}
\usepackage{array}
\usepackage{scalerel}
\usepackage{fontspec}
\usepackage{amsmath}
\usepackage{unicode-math}
\usepackage{mdframed}
\usepackage{wrapfig}
\usepackage{xspace}
\usepackage[group-separator={,}]{siunitx}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage[colorlinks=true,linkcolor=blue,unicode=true]{hyperref}
\usepackage{tikz}
\usepackage{xcolor,cancel}
\usepackage{titlesec}
\usepackage{titletoc}

\newlength{\digitWidth}
\settowidth{\digitWidth}{6}

\newcommand*\circled[1]{\tikz[baseline=(char.base)]{
    \node[shape=circle,draw,inner sep=2pt] (char) {#1};}}

% Set the default for section to start with "Level" in both the
% document and the table of contents
\titleformat{\section}%
{\Large \bfseries}{Level \thesection}{0.5em}{}
\titlecontents{section}%
 [0em] % left
 {\large \addvspace{.5em}} % above
 {\large \bfseries Level \thecontentslabel\ } % before with label
 {\bfseries \hspace{-0em}} % before without label
 {\hfill\contentspage} % filler and page

% Set the depth of the table of contents to show only \section level
\setcounter{tocdepth}{1}

% Sets up the font
\setmainfont[Ligatures=TeX]{texgyrebonum}[
   Path =../../typefaces/,
   Extension = .otf,
   UprightFont = *-regular,
   BoldFont = *-bold,
   ItalicFont = *-italic,
   BoldItalicFont = *-bolditalic]

\setmathfont{texgyrebonum-math.otf}[
   Path = ../../typefaces/]
   
\defaultfontfeatures{
  Ligatures={TeX}
}

% Define acronyms that I use
\newcommand{\acronym}[1]{\textsc{#1}\xspace}
\newcommand{\fs}{\textsc{fs}\xspace}

% Define a horizontal bar to cross out text, like strikethrough
\newcommand\hcancel[2][black]{\setbox0=\hbox{$#2$}%
  \rlap{\raisebox{.45\ht0}{\textcolor{#1}{\rule{\wd0}{1pt}}}}#2} 

% Define a marginpar that is ragged on the outside of the page.
\newcommand{\mymarginpar}[1]{%
  \marginpar[\raggedleft#1]{\raggedright#1}}

% frames for example problems in the math text
\mdfdefinestyle{frexample}{skipabove=.7\baselineskip,%
  skipbelow=.3\baselineskip, innertopmargin=.3\baselineskip,%
  innerbottommargin=.85\baselineskip}

% frames for example word problem 
\mdfdefinestyle{wpexample}{skipabove=0.5\baselineskip,%
  skipbelow=.5\baselineskip, innertopmargin=.3\baselineskip,%
  innerbottommargin=.7\baselineskip,
  userdefinedwidth=.8\textwidth, align=center,
  roundcorner=24pt}

% font for decorative elements
\newfontface\scripty
[Contextuals={WordInitial,WordFinal}]
{TeX Gyre Chorus}

% COMMANDS 
\newcommand{\thetitle}{%
Desert Willow Family School\\
Fractions 2 Class Notes}
\newcommand{\theauthor}{{\scripty by} Park Hays {\scripty from} Gael Keyes}

% Set up the headers for the first section
\pagestyle{fancy}
\fancyhead[LE,RO]{INTRODUCTION}
\fancyhead[LO,RE]{}
\fancyfoot[C]{\thepage}

% A counter for the examples. Reset it on each section.
\newcounter{examplenum}

\begin{document}
\title{\thetitle}
\author{\theauthor\\
\texttt{parkhays@gmail.com} \quad \texttt{keyes@aps.edu}}
\maketitle
{\footnotesize \noindent \textbf{Copyright \copyright 2014 Gael Keyes
    and Park Hays}. Permission is granted for duplication and use by
  Desert Willow Family School staff and parents or guardians of Desert
  Willow Family School students. Permission may be granted to others
  on request.}
\tableofcontents
\clearpage


\section*{Introduction}
\label{sec:introduction}
\addcontentsline{toc}{section}{Introduction}

The Fractions 2 note set does not stand alone. Instead it builds on
the Family School (\fs) Fractions 1 curriculum. Fractions 1 taught
only non-negative fractions, values from zero to one. Fractions 2
extends this to the right on the number line by considering fractions
larger than one. We teach mixed numbers, like $2 \frac{1}{2}$ as well
as improper fractions like $\frac{9}{4}$.

Typically \fs note sets include a guide to teaching, but this set does
not. The guidance in Fractions 1 applies to Fractions 2 without much
modification. Please re-read those sections of the Fractions 1 note
set.

As\mymarginpar{Attend the parent classes, they are the \emph{best} way
  to learn this curriculum.} with the other classes, ensure your
student does not advance to the next level until he or she has
demonstrated a complete grasp of a level. Most students should be
taught the material in the order presented. If coming into the \fs
curriculum from another curriculum, your student will be taught
starting from his or her current knowledge working quickly
\emph{backward}. Please consult your student's \fs teacher in these
cases.

\begin{table}[htbp]
  \caption{Guide to Levels}
  \label{tab:guide_to_levels}
  \vspace{.5\baselineskip}
  \begin{tabular}{clc}
    Level & Content & Lessons \\
    \hline
    1 & From mixed numbers to improper fractions & 1 \\ 
    2 & Adding mixed and improper fractions & 1 \\ 
    3 & Subtracting mixed and improper fractions & 1 \\
    4 & Multiplying mixed and improper fractions & 1 \\ 
    5 & Dividing mixed and improper fractions & 1 \\ 
    6 & Mixed operations and solving & 1 \\ \hline
  \end{tabular}
\end{table}

\section{From Mixed Numbers to Improper Fractions}
\label{sec:mixedtoimproper}
\setcounter{examplenum}{1}
\fancyhead[LE,RO]{\slshape \leftmark} 
\fancyhead[LO,RE]{}
\fancyfoot[C]{\thepage} 

In this level students learn to convert between mixed numbers and
improper fractions. To change a mixed number into an improper fraction
using manipulatives your student will need whole blocks. Select from
any fully-shaded blocks to represent whole numbers as in the following
example 
\begin{center}
  \includegraphics[scale=1.25]{graphics/fig_7over3-with-manipulatives}
\end{center}
It is also possible to think of this as
\begin{center} 
  \includegraphics[scale=1.25]{graphics/fig_7over3-with-nameofone}
\end{center}

Have the student do a night of homework converting mixed numbers by
writing out the problem as in:
\begin{mdframed}[style=frexample]
\noindent \textbf{Example \arabic{section}.\arabic{examplenum}}%
  \quad Convert $3 \frac{1}{4}$ to an improper fraction.
  \stepcounter{examplenum}

\noindent \textbf{Solution}
\[
\frac{4}{4} + \frac{4}{4}  +\frac{4}{4} + \frac{1}{4} = \frac{13}{4}
\]
\end{mdframed}

\subsection{Converting Improper Fractions to Mixed Numbers}
Reversing the process is simply a matter of doing the division. You
may need to remind your student that the portion left (the remainder) is
the numerator of the fraction.
\begin{mdframed}[style=frexample]
  \noindent \textbf{Example \arabic{section}.\arabic{examplenum}}%
  \quad Convert $\frac{13}{4}$ to an mixed number.
  \stepcounter{examplenum}

  \noindent \textbf{Solution}
  \begin{center} 
    \includegraphics[scale=1.25]{graphics/fig_thirteen-fourths}
  \end{center}
\end{mdframed}

\section{Adding Mixed and Improper Fractions}
\label{sec:adding}
\setcounter{examplenum}{1}
Adding improper fractions is the same as adding ordinary
fractions. Don’t forget to simplify.

Adding mixed numbers may require carrying from the fractions into the
whole numbers. Once the fractional parts have common denominators the
student asks “when I add these is it going to make a whole?” Take that
amount from one of the fractions, and carry the whole.
\begin{mdframed}[style=frexample]
  \noindent \textbf{Example \arabic{section}.\arabic{examplenum}}%
  \quad What is $3 \frac{4}{5} + 2 \frac{3}{4}$?
  \stepcounter{examplenum}

  \noindent \textbf{Solution}

  Write the mixed numbers stacked so that their fraction parts
  align. Convert the fraction parts to have a common denominator of
  20. Start thinking about the fraction part, when added will they
  make a whole? Yes. How much must be added to $\frac{16}{20}$ to make
  a whole? $\frac{4}{20}$. If I get those $\frac{4}{20}$ from the
  $\frac{15}{20}$ part, how many will be left? $\frac{11}{20}$. Make
  the whole and carry it to the ones column. The remaining portion is
  all that is left.
  \begin{center} 
    \includegraphics[scale=1.25]{graphics/fig_adding-mixed}
  \end{center}
  After adding the answer is $6 \frac{11}{20}$. Since $\frac{11}{20}$
  cannot be simplified, this is the final answer.
\end{mdframed}

\section{Subtracting Mixed and Improper Fractions}
\label{sec:subtracting}
\setcounter{examplenum}{1} Subtracting improper fractions is
straightforward, it is the natural partner of adding improper
fractions and uses the same process learned in Fractions 1. Mixed
fractions are more challenging, because they require borrowing from
the whole number into the fraction parts. The first step in solving a
problem is to convert the fraction parts to their equivalent with the
least common denominator. Next, borrow the value of one from the top
term's whole numbers as a \emph{name of one}. Consider the next
example.
\begin{mdframed}[style=frexample]
  \noindent \textbf{Example \arabic{section}.\arabic{examplenum}}%
  \stepcounter{examplenum}
  \[ 5 \frac{2}{9} - 2 \frac{5}{6}\]

  \noindent \textbf{Solution}

  First, convert the fractional parts to have the least common
  denominator of 18. $\frac{2}{9}$ becomes $\frac{4}{18}$ and
  $\frac{5}{6}$ becomes $\frac{15}{18}$. Since the bottom
  $\left(\frac{15}{18}\right)$ is larger than the top
  $\left(\frac{4}{18}\right)$ we will have to borrow.
  \begin{center}
    \includegraphics[scale=1.25]{graphics/fig_subtracting-mixed1}
  \end{center}
  To borrow we take one from the top number's whole part, and add it's
  name of one $\left(\frac{18}{18}\right)$ to the top's
  $\frac{4}{18}$. The total, $\frac{22}{18}$, is an improper
  fraction. Subtract the bottom fraction, to get the fraction part
  $\frac{7}{18}$ and subtract the whole parts to get 2.
\end{mdframed}
Writing an improper subtraction problem that requires borrowing
requires that the bottom number's fraction be greater than the top
number's fraction. One easy way to create these problems is to make
the top fraction less than one half and the bottom fraction greater
than one half.
\begin{center}
  \includegraphics[scale=1.25]{graphics/fig_subtracting-make-borrowing}
\end{center}

\section{Multiplying Mixed and Improper Fractions}
\label{sec:multiplying}
\setcounter{examplenum}{1}
A common first-try student might answer the following problem
\[
3 \frac{1}{3} × 4 \frac{2}{7} →
\underbrace{12 \frac{2}{21}}_{\text{typical kid answer}}
\]
The student who gives this answer might think this is three times four
and one third times two sevenths. You can explain how it would be
invalid to use the same thinking with ordinary two-digit integers, as
in the product of 23 and 34 is not $2×3$ and $3×4$.
\begin{center} 
  \includegraphics[scale=1.25]{graphics/fig_23times34}
\end{center}
Always start by challenging your student with the stacked version of
the product. Expect it to be too big a challenge. If they execute it,
it is a lot of operations, as in
\begin{center} 
  \includegraphics[scale=1.25]{graphics/fig_fraction-foil}
\end{center}
It \mymarginpar{These problems have many steps, encourage students to
  practice their mental math.}  is important for students to
understand the process, but teach them that it is less work to convert
to improper fractions, multiply, and then simplify. To continue the
previous challenge with an example
\begin{center} 
  \includegraphics[scale=1.25]{graphics/fig_fraction-improper}
\end{center}

\subsection{Estimation and Checking}
Students need to be familiar with the consequence of multiplying
fractions, improper fractions and mixed numbers. The rules are:
\begin{itemize}
\item Integer times an integer $\Rightarrow$ a larger integer
\item Fraction times a fraction $\Rightarrow$ a smaller fraction
\item Mixed number times a mixed number $\Rightarrow$ a larger number,
  which may be mixed
\end{itemize}
Have your students demonstrate this with a few example problems to
help internalize it.

\subsection{Word Problems}
When you are writing word problems, and when your student is solving
word problems with mixed numbers, think through the problem
substituting whole numbers to reduce the confusion.

\begin{mdframed}[style=frexample]
  \noindent \textbf{Example \arabic{section}.\arabic{examplenum}}%
  \quad Billy fills a $1 \frac{1}{2}$ quart pitcher to the top two and
  half times. How much water did he use?
  \stepcounter{examplenum}

  \noindent \textbf{Solution} Re-think the problem to read: Billy
  fills a $\hcancel[red]{1 \frac{1}{2}}$ 1 quart pitcher to the top
  \hcancel[red]{two and half} 2 times. How much water did he use?
\end{mdframed}

The same advice applies to any fraction word problem. Integers make it
easier to understand the mechanics of the problem.

\section{Dividing Mixed and Improper Fractions}
\label{sec:dividing}
\setcounter{examplenum}{1}
Division is like multiplication, but remember that we are multiplying
the numerator and denominator by the reciprocal of the
denominator. The reciprocal of a number $a$ is what you multiply times
$a$ to make a product of one. As in
\[
\frac{3 \frac{2}{3}}{4 \frac{4}{5}} =  
\frac{ \frac{11}{3}}{\frac{24}{5}} = 
\frac{\frac{11}{3}⋅\frac{5}{24}}{\frac{24}{5}⋅\frac{5}{24}}   =
\frac{11}{3}\times \frac{5}{24}   =   
\frac{55}{72}
\]

Division is easy once students understand multiplication of mixed
numbers.

\section{Mixed Operations and Solving}
\label{ref:mixed_operations}
\setcounter{examplenum}{1}
Practice on mixed number and improper fraction problems. Some key
problems to include have the form
\[
\frac{a}{b-c}.
\]

A special case is when $b-c=0$, where the intermediate result has the
form
\[
\frac{a}{0}=\textrm{undefined}.
\]

Students \emph{must} write “undefined”. If you are interested in
teaching or understanding \emph{why} a division by zero is undefined,
Sal Kahn has an explanation on
\href{https://www.khanacademy.org/math/trigonometry/functions_and_graphs/undefined_indeterminate/v/why-zero-divided-by-zero-is-undefined-indeterminate}{Khan
  Academy}\footnote{\url{https://www.khanacademy.org/math/trigonometry/functions_and_graphs/undefined_indeterminate/v/why-zero-divided-by-zero-is-undefined-indeterminate}}.

We also like to set up problems with many steps.
\begin{mdframed}[style=frexample]
  \noindent \textbf{Example \arabic{section}.\arabic{examplenum}}%
  \stepcounter{examplenum}
  $\frac{3}{4} \left(x+\frac{2}{3}\right) = \frac{2}{7} 
  +3\left(\frac{1}{2}-\frac{1}{3}\right)$
\end{mdframed}
Or if your students are less familiar with algebra a simpler problem
might have the form
\begin{mdframed}[style=frexample]
  \noindent \textbf{Example \arabic{section}.\arabic{examplenum}}%
  \stepcounter{examplenum}
  \noindent $\frac{3}{4}+x=\frac{2}{3}×\left(4 \frac{1}{2}\right)$

\end{mdframed}

Students will also be tested on ordering tricky problems and
greater-than, less-than, equals ($>$,$<$,$=$), as in

\begin{mdframed}[style=frexample]
  \noindent \textbf{Example \arabic{section}.\arabic{examplenum}}%
  \stepcounter{examplenum}
  \quad Place the following fractions in order from least to greatest:
  \[
  3 \frac{1}{2},\quad   3 \frac{5}{8},\quad   2 \frac{7}{5}
  \]
\end{mdframed}
Notice that the last number is both \emph{mixed} and
\emph{improper}. To solve these, convert them all to improper
fractions with a common denominator.

% Teach ratios at this phase too. Ratios are \emph{not} exactly
% fractions. The ratio of red balls to green balls is 2:3 does not say
% that $\frac{2}{3}$ of the balls are red. Rather, it says that
% $\frac{2}{5}$ of the balls are red.

% <<2014-1-5: question in to Gael on what to teach with ratios>>

\section*{Bonus Material --- Introducing Decimals}
\label{sec:itroducing_decimals}
\addcontentsline{toc}{section}{Bonus Material --- Introducing
  Decimals}
\setcounter{examplenum}{1}
Decimals have a Mobius Math lesson. The basic connection is
understanding that place value continues to the right of the decimal
point.
\begin{center} 
  \includegraphics[scale=1.25]{graphics/fig_introducingdecimals}
\end{center}

Some students will want to see symmetry, and will look for the
“oneths” place. If you have students with some familiarity with
exponents, you can show the same number line in exponent form. Notice
the symmetry is around the ones place, not around the decimal point.
\begin{center} 
  \includegraphics[scale=1.25]{graphics/fig_decimalsasexponents}
\end{center}
Decimal problems can be thought of as fraction problems. We typically
teach these decimal manipulations around fifth grade, but teach it
when your students are ready. Adding decimals, as in $28.2+1.23$, means
adding ones to ones, tenths to tenths, and so on. In a sense, each
decimal number represents several fractions added together, all of
which use common denominators. Still, to understand the relationship
with fractions, have students spend a night writing out the problems
in fraction form. 

\subsection{Decimal Multiplication}

Multiplication of decimals can always be treated as an equivalent
multiplication of fractions problem. Consider the following decimal
multiplication problem:
\begin{center} 
  \includegraphics[scale=1.25]{graphics/fig_decimalmultiplication}
\end{center}

\subsection{Decimal Division}
Revisit division with the question “how many $x$’s are in $y$?” For
example, in 
\begin{center}
  \includegraphics[scale=1.25]{graphics/fig_decimaldivision}
\end{center}
Both the divisor and dividend can be multiplied by 10 which moves the
decimal to the right. Refresh students by showing common problem, like
\begin{center}
  \includegraphics[scale=1.25]{graphics/fig_similardivision}
\end{center}

With decimal division, we teach that students need to show two decimal
places, but calculate the third and then round. I teach to use digits
of precision to match the number of the most precise value, and then
calculate one additional value and round.

Inevitably, the student will do a problem like convert $1÷3$ to a
decimal. The result, infinitely repeating 0.33333, can be expressed
with $0.\overline{33}$. The line over the is called a repeating
decimal and indicated with an overbar or overline.

\subsection{Converting Terminating Decimals to Fractions}
\label{sec:convertTerminateDecToFrac}
A terminating decimal is one that ends (terminates) without an
infinite number of digits. We deal with converting non-terminating
decimals to fractions in a later section. To convert a terminating
decimal, such as 0.14, to a decimal, we have to figure out how to
write this as a fraction. Remember that a 0.1 is the same as 1/10 and
0.04 is the same as 4/100 we can say
\begin{equation*}
  0.14 = \frac{ 1}{10} + \frac{4}{100}.
\end{equation*}
To write this we just have to look back at decimal place
value. Converting this to a fraction only requires our knowledge of
fractional place value. We have to remember 0.1 is the same thing as
saying ``one in the tenths place'' or 1/10. The same thing as saying
that ``0.04 is four in the hundredths place'' or 4/100. After writing
the decimal as fractions, adding these proper fractions should be
familiar, namely
\begin{equation*}
  0.14 = \frac{ 1}{10} + \frac{4}{100} =
  \frac{1}{10}×\frac{10}{10} + \frac{4}{100} =
  \frac{10}{100} + \frac{4}{100} = \frac{14}{100}.
\end{equation*}

While this \emph{is} a fraction, we aren't done. The fraction must be
simplified! 14/100 simplifies to 7/50. We know 7/50 is fully simplified
because seven is prime and seven is not a factor of 50.

A shortcut is to find the smallest place value, as in
\begin{equation}
  \label{eq:smallestDecimalPlaceValue}
  0.1\textcolor{red}{4} = \frac{14}{100},
\end{equation}
because $\textcolor{red}{4}$ is in the hundredths place. Here's the
shortcut in a slightly more complex problem
\begin{mdframed}[style=frexample]
\noindent \textbf{Example \arabic{section}.\arabic{examplenum}}%
  \quad Convert $21.35$ to an improper fraction.
  \stepcounter{examplenum}

\noindent \textbf{Solution}
\[
21.35 = 21.3\textcolor{red}{5} = 21 \frac{35}{100} = 21 \frac{7}{20}.
\]
\end{mdframed}
With very little practice your student will find it easy to write the
decimal as a fraction of 10, 100, 1000, and so on. Most of the work is
simplifying the fraction after writing it. Your student will
consolidate his or her skills at fraction simplification through this
process. This technique will not work on fractions like
$0.\overline{3}$ or $0.\overline{0.142857}$ because these are
repeating.

\subsection{Showing that 0.999\dots{} Equals 1}
\label{sec:showingPoint999IsOne}
This short example shows how to convert repeating non-terminating
decimals to fractions. The number $0.\overline{9}$ is the same as
one. It is hardly obvious, how can we show the equality?

First, we use a little bit of algebra, a variable. Fractions are the
gateway to algebra, so this is a good opportunity to introduce the
idea of a variable if your student is not already familiar with
it. Write a simple equation
\begin{equation*}
  \label{eq:simplestRepeating}
  y = 0.\overline{9}.
\end{equation*}
All we did is say that there is some number $y$ and we know that it is
the same thing as the number $0.\overline{9}$. An equation is like a
teeter-totter, if we put a kid on one side, we need to put another kid
on the opposite side or the teeter-totter will be unbalanced. That is,
whatever we do to one side of the equation we must do to the other
or we'll unbalance the equation.

First, multiply both sides of the equation by 10,
\begin{equation*}
  \label{eq:simplestRepeatingTimesTen}
  10×y = 10×0.\overline{9}.
\end{equation*}
We multiplied both sides by ten. That's OK because we did the
\emph{same thing to both sides of the equation}. Ten times
$0.\overline{9}$ is just $9.\overline{9}$. So we can simplify the
equation to
\begin{equation*}
  \label{eq:simplestRepeatingTimesTenSimplified}
  10×y = 9.\overline{9}.
\end{equation*}
Now we subtract $0.\overline{9}$ from both sides. That's OK because,
again, we're doing the same thing to both sides
\begin{equation*}
  \label{eq:simplestRepeatingTimesTenSimplifiedLessX}
  10×y - 0.\overline{9} = 9.\overline{9} - 0.\overline{9}.
\end{equation*}
We will now simplify each side through several steps. Starting with
the right-hand side
\begin{equation*}
  \label{eq:simplestRepeatingRHS}
  9.\overline{9} - 0.\overline{9} =
  \begin{array}{r@{}l}
    9&.\overline{9}\\
    -\quad 0&.\overline{9}\\
    \hline
    9&\\
  \end{array}.
\end{equation*}
With the simplified right-hand side our equation looks like
\begin{equation*}
  10×y - 0.\overline{9} = 9.
\end{equation*}
The left-hand side can be simplified by remembering that $y=0.\overline{9}$, so replace the ``$-0.\overline{9}$'' with $-y$ to get the equation
\begin{equation*}
  10×y - y = 9.
\end{equation*}
If we have ten $y$s and take one $y$ away that leaves nine $y$s, so are equation is now
\begin{equation*}
  9×y = 9
\end{equation*}
Divide both sides by 9,
\begin{equation*}
  \frac{9×y}{9} = \frac{9}{9} \Rightarrow
  y = 1.
\end{equation*}
Remember that in the very first step we defined $y=0.\overline{9}$, and we showed that $y=1$ too. Therefore $y=0.\overline{9} = 1$.

The process we used involved first creating an equation by letting a
variable be equal to our initial repeating decimal. Then we multiplied
both sides of the equation by some integer. Next, we simplified the
equation until we got back to just our variable except that on the
right-hand side we had a fraction whole number instead of a repeating
decimal.

We can convert any repeating decimal into a fraction by following
these steps.

\section{Converting Repeating Decimals to Fractions}
\label{sec:convertRepeatingDecToFrac}
To convert a repeating decimal to a fraction, we start by figuring out
how many digits are in the repeating pattern. Consider these

\begin{center}
  \begin{tabular}{lc}
    Decimal & Digits \\
    $0.\overline{9}$ & 1\\
    $0.\overline{3}$ & 1 \\
    $0.\overline{142857}$ & 6 \\
    $2.\overline{09}$ & 2 \\
  \end{tabular}
\end{center}


Take the decimal and make a small equation with it. Suppose we are given $0.\overline{09}$ as our repeating decimal, then we would start with a simple equation like $y=0.\overline{09}$. Now, multiply by both sides of the equation by 1 with zeros after it. The number of zeros is the same as the number of repeating digits. Continuing the example, see that $0.\overline{09}$ has two repeating digits, so we multiply \emph{both sides} by 100 (a 1 with two zeros after it\footnote{%
  If there are $n$ repeating digits, multiply by $10^n$.}).
\begin{eqnarray*}
  100×y &= 0.\overline{09} × 100 \\
  100×y &= 9.\overline{09}
\end{eqnarray*}

Now subtract $y$ from both sides. The tricky bit is that we use the symbol $y$ on the left and its number value on the right, like
\begin{equation*}
  100×y \textcolor{red}{- y} = 9.\overline{09}\textcolor{red}{ - 0.\overline{09}}.
\end{equation*}

Simplify the left-hand side. While you could use variable factoring,
it is more intuitive at this level to think ``if I have 100 $y$s and I
take one $y$ away, how many $y$s will I have left?''. Also, simplify
the right-hand side by executing the subtraction,
\begin{eqnarray*}
  99×y &=&
  \begin{array}{r@{}l}
    9&.\overline{09}\\
    -\quad 0&.\overline{09}\\
    \hline
    9&\\
  \end{array}\\
  99×y &=& 9\\
\end{eqnarray*}
Finally, divide both sides but whatever is now multiplying $y$ on the left, in this case 99, and simplify
\begin{eqnarray*}
  \dfrac{\cancel{99}×y}{\cancel{99}} &=& \dfrac{9}{99}\\
  y &=& \dfrac{9}{99}\\
  y &=& \dfrac{\cancel{9}}{\cancel{9}×11}\\
  y &=& \dfrac{1}{9}\\
\end{eqnarray*}

Although illustrated with examples, the process is completely
algorithmic. As a final example, we'll consider what to do when
starting with a repeating decimal that includes a whole number. The
algorithm \emph{is the same}.

\begin{mdframed}[style=frexample]
  \noindent \textbf{Example \arabic{examplenum}: repeating decimal with whole number }%
  \stepcounter{examplenum}
  
  Express $3.\overline{6}$ as a fraction.

  \noindent \textbf{Solution}
  
  Make our number into an equation $y=3.\overline{6}$. We have one repeating digit (pay no attention to the 3) so we multiply by 10,
  \begin{equation*}
    10×y = 36.\overline{6}.
  \end{equation*}
  Now subtract $y=3.\overline{6}$ from the equation
  \begin{equation*}
    \begin{array}{r}
      10×y\\
      \underline{-\quad y}\\
      9×y\\
    \end{array}
    =
    \begin{array}{r@{}l}
      36&.\overline{6}\\
      -\quad 3&.\overline{6}\\
      \hline
      33&\\
    \end{array}
  \end{equation*}

  Divide both sides of the equation by whatever is multiplying $y$ (in this case 9)
  \begin{eqnarray*}
    \dfrac{\cancel{9}×y}{\cancel{9}} &=&
                                         \dfrac{33}{9}\\
    y &=& \dfrac{\cancel{3}×11}{\cancel{3}×3}\\
    y &=& 3\dfrac{2}{3}\\
  \end{eqnarray*}
\end{mdframed}

\subsection{Fractional Exponents}
This material was entirely bonus content at the fractions class, it is
not really part of the fractions unit, and consequently is not
required material for you to teach or your students to know. It is,
however, rather interesting.

What is $9^{\frac{1}{2}}$? You might remember that it is the same as
$\sqrt{9}=3$. Whether you remember or not, it is fairly easy to
derive. Consider the simple problem $5^2⋅5^3$. We know that this is the
same as $(5⋅5)⋅(5⋅5⋅5⋅5⋅5)=5^5$. So, when we multiply exponents of the
same base, we add their exponents. Now, extend the idea to a
fraction. In the case of $9^{\frac{1}{2}}$, think about this
\begin{equation*}
  \begin{split}
    9^{\frac{1}{2}}⋅9^{\frac{1}{2}}&=9^1\\
    3⋅3&=9^1
  \end{split}
\end{equation*}
So, it is clear that $9^{\frac{1}{2}}=3$. The rules hold for other fractions
too. Consider $8^{\frac{1}{3}}$—again you may recall that this is the cube root
of 8, but to derive it
\begin{equation*}
  \begin{split}
    8^{\frac{1}{3}}⋅8^{\frac{1}{3}}⋅8^{\frac{1}{3}}&=8^1\\
    2⋅2⋅2&=8\\
    8^{\frac{1}{3}}&=2
  \end{split}
\end{equation*}

\end{document}

% Example frame 
\begin{mdframed}[style=frexample]
\noindent \textbf{Example A.A} \quad Example

\noindent This is the problem

\begin{center}
  \includegraphics[scale=1.25]{graphics/dummy_drawing}
\end{center}
\end{mdframed}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
