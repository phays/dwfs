%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 

\documentclass[letterpaper,11pt,twoside]{article}
\usepackage{array}
\usepackage{scalerel}
\usepackage{fontspec}
\usepackage{unicode-math}
\usepackage{mdframed}
\usepackage{wrapfig}
\usepackage{xspace}
\usepackage{pifont}
\usepackage[group-separator={,}]{siunitx}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{color}
\usepackage[colorlinks=true,linkcolor=blue,unicode=true]{hyperref}
\usepackage{textcomp}
\usepackage{tikz}

% Controls the depth of 
\setcounter{tocdepth}{1}
% Sets up the font
\setmainfont[Ligatures=TeX]{texgyrebonum}[
   Path =../../typefaces/,
   Extension = .otf,
   UprightFont = *-regular,
   BoldFont = *-bold,
   ItalicFont = *-italic,
   BoldItalicFont = *-bolditalic]

\setmathfont{texgyrebonum-math.otf}[
   Path = ../../typefaces/]

\defaultfontfeatures{
  Ligatures={TeX}
}

%\setcounter{secnumdepth}{0}
\mdfdefinestyle{frexample}{skipabove=.7\baselineskip,%
  skipbelow=.3\baselineskip, innertopmargin=.3\baselineskip,%
  innerbottommargin=.85\baselineskip}

% word problem example frame
\mdfdefinestyle{wpexample}{skipabove=0.5\baselineskip,%
  skipbelow=.5\baselineskip, innertopmargin=.3\baselineskip,%
  innerbottommargin=.7\baselineskip,
  userdefinedwidth=.8\textwidth, align=center,
  roundcorner=24pt}

\newfontface\scripty
[Contextuals={WordInitial,WordFinal}]
{TeX Gyre Chorus}


\newcommand{\thetitle}{%
Desert Willow Family School\\
Division Class Notes}
\newcommand{\theauthor}{{\scripty by} Park Hays {\scripty from} Gael Keyes}
\newcommand{\fs}{\textsc{fs}\xspace}
\newcommand{\mydiv}[1]{\strut
     $\,\overline{\vphantom{\big)}%
     \hbox{\smash{\raise3.5\fontdimen8\textfont3\hbox{$\big)$}}}%
     \mkern2mu #1}$}

\pagestyle{fancy}
\fancyhead[LE,RO]{TEACHING TECHNIQUES}
\fancyhead[LO,RE]{}
\fancyfoot[C]{\thepage}

\begin{document}
\title{\thetitle}
\author{\theauthor\\
\texttt{parkhays@gmail.com} \quad \texttt{keyes@aps.edu}}
\maketitle
%\includegraphics{graphics/by-nc-nd}
{\footnotesize \noindent \textbf{Copyright \copyright 2014 Gael Keyes
    and Park Hays}. Permission is granted for duplication and use by
  Desert Willow Family School staff and parents or guardians of Desert
  Willow Family School students. Permission may be granted to others
  on request.}
\tableofcontents
\clearpage

% Debugging, reference sizes
%\makeatletter
%\newcommand\thefontsize[1]{{#1 The current font size is: \f@size pt\par}}
%\makeatother
%\sffamily
%\thefontsize\tiny
%\thefontsize\scriptsize
%\thefontsize\footnotesize
%\thefontsize\small
%Normal \thefontsize\normalsize
%\thefontsize\large
%\thefontsize\Large
%\thefontsize\LARGE
%\thefontsize\huge
%\thefontsize\Huge
%\the\baselineskip
% \lhead[even side]{odd side}

\section*{Teaching Techniques}
\addcontentsline{toc}{section}{Teaching Techniques}%

\label{sec:teaching_techniques}
The Family School (\fs) division curriculum in this document is part
of the \emph{algorithms mode of engagement} within the mathematics
branch. The modes of engagement are discussed in the Family School
\href{http://familyschool.aps.edu/fshandbook.htm#ModesEngagement}{handbook}.
We sometimes think of the engagement modes as a tree, part of which is
shown in figure \ref{fig:modes_of_engagement}. The division
algorithmic classes are only one of the Family School doors into the
house of mathematics.

Parents and \fs teachers both work to educate the
student. Individualized education is a bromide, a clich\'e, but it is
also the best for learning. In the context of the algorithm
curriculum, individualized learning means two specific things. First,
start teaching with what the student knows. Second, only advance to
the next level when the student has fully understood the material in
one level. Individualism is in the sequence the material is taught and
in the pace. The content is not individualized.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=1.25]{graphics/modes_of_engagement}
  \caption{The division algorithm's \emph{mode of engagement} and
    context.}
  \label{fig:modes_of_engagement}
\end{figure}

If your student is new to division, teach this document in the order
of the levels shown. It builds from the knowledge students started
learning with anticounting.  \marginpar{\raggedright{Attending parent
    classes is crucial.}} On the other hand, if your student is new to
\fs and has some division experience then start with their current
level and \emph{work backward}. Reversing the order takes them from
what they are comfortable with backward through the process until they
can understand the \emph{why} behind the processes they may already be
using.


To teach effectively a parent or teacher must understand the
material. Most parents did not learn the \fs curriculum; it is very
important that parents attend the classes offered for parents.

When teaching, a typical lesson lasts about 20~minutes. The lesson
content follows the outline in these notes, but a level or sub-topic
commonly takes more than one lesson. A takes as many lessons as the
student requires. \marginpar{\raggedright{Slow mastery is better than
    speedy coverage.}} After a lesson the student has practice or
homework to exercise the skill learned in the lesson. As a teacher you
are watching to ensure the student understands the material and can
demonstrate the skills without error \emph{before advancing.}

We have attempted to note material that is likely to be on tests.
Please teach the understanding, and not the test.

As a parent or teacher you may be correcting or creating assignments. The
following sections are techniques for teaching the division
curriculum. 

\subsection*{How to Make Friendly Problems}
\label{sec:friendly_problems}
In the beginning of the curriculum students will use the 1, 10, and
100 block manipulatives to solve problems. You can inadvertently
create more work for your student than you intend if the problems are
not carefully constructed. Problems that can be solved easily are, in
this context, friendly. A \emph{single} problem will have a quotient
whose first digit is 1.

\begin{description}
  \item[Tip 1] Fewer trade-ins are more kid-friendly. Remember that a
  trade-in is when you have to exchange a larger block for a group of
  smaller blocks. For example, trading in a 10-stick for 10 unit
  cubes.

  % fig_friendly-probs-1
  \vspace{.5\baselineskip}
  \includegraphics[scale=1.25]{graphics/fig_friendly-probs-1}
  \vspace{.5\baselineskip}

  \item[Tip 2] Make the tens digit of the dividend larger than the minimum
  needed by at least the value in the tens place of the divisor. For
  example, 259 divided by 23 is a \emph{single} problem, so the tens
  place of the dividend (5) is equal to 3 plus the tens place of the
  divisor.

  \vspace{.5\baselineskip}
  \includegraphics[scale=1.25]{graphics/fig_friendly-probs-2}
  \vspace{.5\baselineskip}
  % fig_friendly-probs-2

  To make this problem a double, multiply the divisor's ones place by
  2, $(3 \times 2 = 6)$ and then add the divisor's ten place (or
  more). $6 + 2 = 8.$ A good example is 487 divided by 23. A more
  difficult problem is 477 divided by 23.

  \item[Tip 3] Use numbers less than 5 in the divisor.
    %\vspace{.5\baselineskip}
    %\includegraphics[scale=1.25]{graphics/fig_friendly-probs-3}
    %fig_friendly-probs-3
\end{description}

\subsection*{Division Timed Tests}
\label{sec:timed_test}

Skills can be consolidated with timed tests. Timed tests focus on
division facts. Timed tests are in three stages. In the first stage
(1), the division facts for one-digit divisors are exercised. In the
second stage (2), the division will be imperfect, the student finds
the greatest factor and ignores the remainder. In the final stage (3)
the student provides the full solution with remainder.
\begin{enumerate}
\item $40÷8=5$, 60 problems in 3 minutes
\item $43÷8=5$ (without the remainder), 60 problems in 3 minutes
\item $43÷8=5 r3$, 60 problems in 5 minutes
\end{enumerate}

\subsection*{Fill-in Worksheets}
\label{sec:fill-in_worksheets}
\begingroup
\setlength\intextsep{0pt}
\begin{wrapfigure}[13]{l}{0.34\textwidth}
\includegraphics[scale=.8]{graphics/fig_fill-in-sheet}
\end{wrapfigure}
One tool teachers use is a worksheet containing a bunch of division
problems where the dividend is specified but the divisor is not. These
are called, sometimes, \emph{fill-in} problem worksheets.
\endgroup

A typical sheet might have eight problems on it. The teacher specifies
the divisor to use when handing out the worksheet. The same worksheet
might be given for multiple assignments, but with a different divisor.

An example fill-in problem is shown in the figure. A typical worksheet
would contain eight problems relevent to the student's current skills,
but would have no divisors. An example is provided in the figure.

\subsection*{Question Mark Problems}
\label{sec:question_mark_problems}
Tests may contain \emph{question mark problems}, where a number has
been replaced with an empty box or a question mark, and the student
must identify the correct number by using the context.  To solve these
problems students must understand the process well enough to, in
effect, reverse it.
%todo: include an example question mark problem

\subsection*{Division Word Problems}
\label{sec:word_problems}
There are an infinite variety of division word problems. It is helpful
to have some categories of word problems when creating them for your
student. In particular, we have identified useful kinds of division
word problem, in \emph{write-and-solve} problems the student is given
a little bit of information and must create and solve their own
problem. \emph{Contained in} problems ask how many of something are
contained in or can be taken from a larger group \emph{Missing Factor}
problems specify a product and one factor, and ask the student to find
the other factor \emph{Area} problems are special kinds of other
problems that involve geometric area

\subsubsection*{Write-and-Solve}
\label{sec:write_and_solve}

In write-and-solve problems, a student is given a little bit of
information and asked to construct and solve and solve a division word
problem using the provided information.

\begin{mdframed}[style=wpexample]
\noindent \textbf{Example Write-and-Solve} \quad 

\noindent Use the following information to build and solve a division
problem.

\begin{center}
  58 baskets, 218 apples
\end{center}

\noindent \textbf{Typical answer}: \textit{If you have 218 apples and
  put an equal number of apples in each basket, how many will go in
  each basket?}
\end{mdframed}

\subsubsection*{Contained In}
\emph{Contained in} problems are asking how many of one thing is within
something else.

\begin{mdframed}[style=wpexample]
\noindent \textbf{Example Contained In} \quad 

\noindent I am making goody bags for my party. I want to put three
Candy-Cubes in each bag, and I have a big bag with 312
Candy-Cubes. How many goody bags can I make?
\end{mdframed}

\noindent A somewhat more interesting variation on this problem
concentrates on the remainder. We use both the round-up and round-down
formats.

\begin{mdframed}[style=wpexample]
\noindent \textbf{Example Contained In, Round Down} \quad 

\noindent I am making goody bags for my party. I have a big bag with
207 Candy-Cubes. I want to put three Candy-Cubes in each bag. How many
goody bags can I make?
\end{mdframed}

\noindent Or, consider one where the student must think about the remainder but
round up.

\begin{mdframed}[style=wpexample]
\noindent \textbf{Example Contained In, Round Up} \quad 

\noindent I am making goody bags for my party. I have a big bag with
207 Candy-Cubes. Each bag can hold up to three Candy-Cubes. How many
bags do I need to use up all the Candy-Cubes?
\end{mdframed}

\noindent Finally, consider the remainder itself as the objective of
the problem.

\begin{mdframed}[style=wpexample]
\noindent \textbf{Example Contained In, Remainder} \quad 

\noindent I am making goody bags for my party. I have a big bag with
207 Candy-Cubes. Each bag gets three Candy-Cubes. Mom said I could eat
any leftovers after I made all the bags I could. How many
Candy-Cubes do I get to eat?
\end{mdframed}

\subsubsection*{Missing Factor}
\label{sec:missing_factor}

In missing factor problems a student is given the product and one
factor, and must calculate the other factor. Very natural problems may
come from geometric area (see below) but you can use more
sophisticated units, as the following problem does.

\begin{mdframed}[style=wpexample]
\noindent \textbf{Example Missing Factor}

\noindent My car gets 36 miles per gallon fuel economy. My car is
empty, and I have to drive 72 miles to camp in the Jemez. How much
gasoline do I need to buy?
\end{mdframed}

\noindent The problem can be made considerably more challenging by
introducing more information and requiring more steps.

\begin{mdframed}[style=wpexample]
\noindent \textbf{Example Harder Missing Factor}

\noindent My car gets 36 miles per gallon fuel economy. My car is
empty, and I have to drive 72 miles to camp in the Jemez. I have \$9,
and gas costs \$4 per gallon. Do I have enough money? If not, how much
more do I need? If so, how much will I have left over?
\end{mdframed}

\subsubsection*{Area Problems}
\label{sec:area_problems}

Area problems are a special category of either \emph{contained in} or
\emph{missing factor} problems. The distinction is that they involve
calculations about geometric area. Consider the following examples
of a \emph{contained in} and a \emph{missing factor} area problem.

\begin{mdframed}[style=wpexample]
\noindent \textbf{Example Missing Factor Area} \quad 

\noindent I have a 144 square inch board, but it is in the material
rack and I can only get to one end. I measure the width of the end and
it is 8 inches wide. How long is the board?
\end{mdframed}

\noindent The next example shows how an area problem can also be a
\emph{contain in} problem. The problem has two steps. It requires the
student first to calculate the total area, then divide by the known
factor, then round off the remainder.

\begin{mdframed}[style=wpexample]
\noindent \textbf{Example Contained In Area} \quad 

\noindent I have a 1-yard wide piece of fabric that is 14 inches
long. I'm making bookmarks that each require 4 square inches of
fabric. How many can I make?
\end{mdframed}
\clearpage

\section{Level 1 --- Building}
\fancyhead[LE,RO]{\slshape \rightmark}
\fancyhead[LO,RE]{\slshape \leftmark}
\fancyfoot[C]{\thepage}

Level 1 is divided into two steps. The steps are taught in order, but
a student could learn a second step before they
have mastered a prior step. He or she should not advance a
level until mastery is demonstrated.

The student first learns to read and interpret the division
symbols. He or she also learns how to build the problem with
blocks. The process of building connects division to the \fs
multiplication curriculum. The order of building is connected to the
multiplication process.

Students will be tested on the connection between multiplication and
division. We call a pair of equations a fact family, as in
\[
\overbrace{a}^{\mathrm{dividend}}
\div 
\underbrace{b}_{\mathrm{divisor}} = \overbrace{c}^{\mathrm{quotient}} 
\quad\quad \underbrace{ b\times c}_{\mathrm{factors}} = 
\underbrace{a}_\mathrm{product}.
\]
Students should be able to construct a fact family and label its
parts. 

\subsection{Step 1 - Reading and Building}
\label{sec:lev1_step1}
There are three signs in division, $12\div 4$, the vinculum
(horizontal bar) $\frac{12}{4}$, and the long division symbol
4\mydiv{12}. Division is thought about by asking \emph{how many times}
the divisor \emph{goes into} the dividend.
% Figure, outside wrapped, fig_goes-into
\begin{center}
  \includegraphics[scale=1.25]{graphics/fig_goes-into}
\end{center}
Our brain thinks “into”, as in “how many 8s go into 32?” or ``how many
times does 8 go into 32?''  

Ask your students what does “goes into” mean?  What does “divided by”
mean?  Listen for conceptual understanding.  Does it mean you want to
build 12s until you get to 287 (correct)?  Does it mean you want to
break 287 into 12 groups that are all the same size (also correct)?
Or, do you want to get 287 and break it into 12s (incorrect)? Why?
Some possible answers include “you might lose track” or “you might
have left-overs”.

\subsection{Step 2 - Build, Write, and Answer}
In build, the student actually solves the problem by building it with
blocks. It is easiest to show with an example.

\begin{mdframed}[style=frexample]
\noindent \textbf{Example 1.1} \quad Build and solve $287\div 12$

\noindent Build the problem to 287. Build across so that there are as
many columns as the divisor (12), and build down until the total
number of unit blocks is the dividend (287). The quotient is the
number of rows. If there is an incomplete row at the end, it is the
remainder (11).

%   Figure, outside wrapped, fig_build-ex-1
% todo: use smaller scale for blocks
\begin{center}
  \includegraphics[scale=1.25]{graphics/fig_build-ex-1}
\end{center}
\end{mdframed}
\clearpage

\begin{mdframed}[style=frexample]
\noindent \textbf{Example 1.2} \quad Build and solve $436 \div 21$

How will you use 21 when you build? Answer: it will be the \emph{over}
or across size.

Build down until you exhaust the original blocks. If you have a group
of blocks at the bottom without enough columns, it is the
remainder.

% todo: scale down graphic
% Figure: fig_build-ex-2 
\begin{center}
  \includegraphics[scale=1.25]{graphics/fig_build-ex-2}
\end{center}

\end{mdframed}
\clearpage
\begin{mdframed}[style=frexample]
\noindent \textbf{Example 1.3} \quad Build and solve $274 \div 23$

Build starting with the largest groups you can. In this case it is the
10s group of 23 (ten tall, 23 wide, totals 230 blocks). Then work down
using smaller groups at each step until there aren’t enough blocks to
complete a row.  The process is already starting to flow like the long
division algorithm, where the dividend is digested starting with its
most significant values.

% Figure: fig_bulid-ex-2
\begin{center}
  \includegraphics[scale=1.25]{graphics/fig_build-ex-3}
\end{center}
\end{mdframed}

Students must be able to build any problem, draw what they built, and
show conceptual understanding of the relationship between
multiplication and division building.

\clearpage
\section{Level 2 - Connect the Blocks to the Algorithm}
\label{sec:lev2}

In Level 2 we introduce the algorithm for division \emph{and connect
  that algorithm with the previous steps}. In many cases grown-ups can
do the algorithm but may not understand why it works. Parent
classes are the best places to learn this process, but we can
summarize it by saying that division is a step by step process where
at each step we reduce the dividend by the most  100 (for example)
groups we can. Having accounted for those, we reduce the leftover by
the largest number of 10 groups of the divisor we can. It is much
easier to show with examples.

\clearpage
\begin{mdframed}[style=frexample]
  \noindent \textbf{Example 2.1} \quad Solve \( 248 \div 11 \)

  \noindent Problems are built by asking for the “largest 100 group of
  11”, for example, and then repeating the process with the remaining
  unit blocks.

  Get 248 blocks. We’re not looking for 11s, instead we are looking
  for our biggest groups of 11.

  Can we build a 100 group of 11 out of 248? A 100 group of 11 is
  \num{1100}, which is too big, so we cannot. Write an “x” over the
  hundreds place (above the 2).

  Can we build a 10 group of 11 out of 248? A 10 group of 11 is
  110. We can build a 10 group of 11. How many 10 groups of 11 can we
  build? Two! Two ten groups of 11 is 20 11s or 220.  Take that group
  away.

  Can we build a one group of 11?  Yes. How many one groups of 11 can
  we get from 28? Two! Take that away, and how many are left? The
  remainder is 6.

% Figure: fig_connect-blocks-to-algo-1
\begin{center}
  \includegraphics[scale=1.25]{graphics/fig_connect-blocks-to-algo-1}
\end{center}

\end{mdframed}
\clearpage
\begin{mdframed}[style=frexample]
  \noindent \textbf{Example 2.2} \quad Solve \( 292 ÷ 12 \)

  \noindent Get 292 blocks. We’re not looking for 12s, instead we are
  looking for our biggest group of 12.

  Can we build a 100s group of 12 out of 292? A 100s group of 12
  is 1200, which is too big, so we cannot. Write an “x” over the
  hundreds place (above the 2).  

  Can we build a 10s group of 12 out of 292? A 10s group of 12 is
  120. We can build a 10s group of 12. How many 10s groups of 12 can
  we build?  Two! A tens group of 12 is 120, and we can make two of
  them.  Take that group away.

  Can we build a ones group of 12 out of 52?  Yes, since a ones group
  of 12 is just 12, and 12 is less than 52. How many ones groups of 12
  can we get from 52?  Four. Take that away, and how many unit blocks
  are left? The final quotient is the number of rows (24) and the
  remainder (4), or 24 r4.

  % fig_connect-block-to-algo-2
 \begin{center}
   \includegraphics[scale=1.25]{graphics/fig_connect-blocks-to-algo-2}
 \end{center}
\end{mdframed}
\clearpage
\begin{mdframed}[style=frexample]
  \noindent \textbf{Example 2.3} \quad Solve $471 ÷ 22$

  \noindent Get 471 blocks. We’re not looking for 22s, instead we are
  looking for our biggest group of 22.

  Can we build a 100s group of 22 out of 471? A 100s group of 22 is
  2200, which is too big, so we cannot. Write an “x” over the hundreds
  place (above the 4).
  
  Can we build a 10s group of 22 out of 471? A 10s group of 22 is
  220. How many 10s groups of 22 can we build? Two! A tens group of 22
  is 220, and we can make two of them.  Take that group away.
  
  Can we build a ones group of 22 out of 31? Yes, since a ones group of
  22 is just 22, and 22 is less than 31. How many ones groups of 22 can
  we get from 31? One. Take that away, and how many unit blocks are
  left?

  % fig_connect-blocks-to-algo-3
  \begin{center}
    \includegraphics[scale=1.25]{graphics/fig_connect-blocks-to-algo-3}
  \end{center}
\end{mdframed}
\clearpage

\section{Level 3 - Long Method - 2 into 4 Digits}
\label{sec:without_blocks}

\begin{wrapfigure}{r}{0.4\textwidth}
  \begin{mdframed}[style=default]
    \begin{center}
      \includegraphics[scale=1.25]{graphics/fig_rounded}
    \end{center}
    \raggedright{
    Teach that place value can be temporarily ignored because
    of rounding.}
    % \caption{\label{fig:frog1}This is a figure caption.}
  \end{mdframed}
\end{wrapfigure}
\noindent In level three the student repeats each part of the
algorithm, but without blocks. The multiplication step is still
performed to the side of the division “stack”; however, the
subtraction steps are starting to look like the long division most
parents learned.

One key concept that students start understanding at this level is how
to estimate the number of groups. We teach this by working with
rounded numbers, temporarily ignoring place value. For example, all of
the division problems in the sidebar have the same quotient. If the
student can focus on 6 divided by 2, then all the problems are
straightforward.

\begin{mdframed}[style=frexample]
  \noindent \textbf{Example 3.1} \quad Solve $5487 ÷ 25$

  Start by working from the left. How many times does 25 go into 5?
  None, of course, so write an x over the 1000s place. Bring another
  digit of the dividend into consideration. How many hundred groups of
  25 go into 5400?  Two. Now we have to remember our place value—what
  are we building? We are building 100s groups of 25, in this case two
  of them. The student must then understand that he or she is taking
  away two 100s groups of 25. The student determines that there are
  200 times 25 in the group (5000) and subtracts that.

  Continue working the 10s groups on the remaining 487. How many tens
  groups of 25 can come from 487? It is easier to see that only one
  group of 25 can come from 48, and then figure that one 10s group of
  25 is 250—subtract that.  

  How many 1s groups of 25 can come from 237?
  
  % todo: improve graphic by multiplying out a 25x2 set.
  % fig_level3-ex-1
  \begin{center}
    \includegraphics[scale=1.25]{graphics/fig_level3-ex-1}
  \end{center}
\end{mdframed}

\begin{mdframed}[style=frexample]
  \noindent \textbf{Example 3.2} \quad Solve $7472 ÷ 36$
  
  \noindent How many 1000 groups of 36 go into 7472?
  None. How many 100 groups of 36 go into 7472? Two, or 7200, so
  subtract 7200.

  How many 10 groups of 36 go into 272? None. How many 1 groups of 36
  go into 272? Suppose the student guesses 6. He or she calculates
  that 6 groups of 36 is 216, subtracts 216 from 272 and gets
  56. \emph{Whoa}, 56 is bigger than 36, so we can get another group
  out. Notice that instead of recalculating the multiplication, we
  just add one more ``1 group of 36'' to 216, get 252, and the
  remainder is less than 36.
 
  % fig_level3-ex-2
  \begin{center}
    \includegraphics[scale=1.25]{graphics/fig_level3-ex-2}
  \end{center}
\end{mdframed}
\clearpage
\section{Level 4 - Long Method - 3 into 6 Digits}
\label{sec:lev4}

\begin{mdframed}[style=frexample]
  \noindent \textbf{Example 4.1} \quad Solve $589023 ÷ 123$

  \noindent Ask “my first group is going to be 123 with how many
  zeros?” The initial answer is 5 zeros, then we have to reduce that
  until the result is the right number of digits.

  Continue working the same process on the amount remaining after each
  step.

  % fig_level4-ex1
  \begin{center}
    \includegraphics[scale=1.25]{graphics/fig_level4-ex-1}
  \end{center}
\end{mdframed}
\clearpage

\section{Level 5 - Side-by-Side Method}
In the side-by-side method we teach the short-cut method
\emph{connected to} the long method learned in levels 3 and 4. The
short-cut method is different because multiplication is done \emph{in
  place}. We do not want students to ``drop down'' values without
understanding that they are using a short cut to writing zeros.

\begin{mdframed}[style=frexample]
  \noindent \textbf{Example 5.1} \quad Solve $\num{4817} ÷ 25$ using the
  side-by-side method

  % fig_side-by-side
  \begin{center}
    \includegraphics[scale=1.25]{graphics/fig_side-by-side}
  \end{center}
\end{mdframed}
\clearpage

\section{Level 6 - Short-cut Method}
In Level 6, place value is used to replace the process of writing
individual multiplication problems and problem stacks. Instead, the
student works with the fewest number of the dividend’s most
significant figures as possible. This, really, is the long division
algorithm.

We \emph{drop down} numbers, but to really understand the algorithm
the student must realize that numbers do not drop down magically. We
are really following the same process learned in level 3, but we are
working with rounded numbers.

\begin{mdframed}[style=frexample]
  \begin{minipage}{\linewidth}
    \begin{wrapfigure}{r}{0.19\textwidth}
      \includegraphics[scale=1.25]{graphics/fig_short-cut-ex1}
    \end{wrapfigure}
    \noindent \textbf{Example 6.1} \quad Solve $\num{4817} ÷ 25$

    Repeating side-by-side example with just the short cut. Does 25 go
    into 4? No. Does 25 go into 48? Yes, how many times? Once. Write
    the 1, multiply 1 by 25 and write that under the 48. Subtract, and
    “drop down” the 1 digit. Now we have the problem 25 into 231.

    Important: when you're finding a digit of the quotient, you
    are working with the corresponding digit of the dividend. In the
    example, when you are finding the 2 in 192 you are working with
    the ones place. You will have dropped the 7 (the dividend’s ones
    place value) and can think of a column of numbers aligned under
    the digit you’re solving.
  \end{minipage}
\end{mdframed}

\begin{mdframed}[style=frexample]  
  \begin{minipage}{\linewidth}
    \begingroup
    \setlength\intextsep{0pt}
    \begin{wrapfigure}{r}{0.3\textwidth}
      \begin{center}
        \includegraphics[scale=1.25]{graphics/fig_short-cut-ex2}
      \end{center}
    \end{wrapfigure}
    \noindent \textbf{Example 6.2} \quad Solve $\num{589023} ÷ 123$

    Does 123 go into 5? No. Does 123 go into 58? No. Does 123 go into
    589? Yes, how many times? Four. Write the 4, multiply 4 by 123 and
    write that under the 589. Subtract, and “drop down” the 0
    digit. Now we have the problem “123 into 861”, which has the
    answer 7. Write the 7, multiply to get 984, and subtract to get
    108. Drop the 3. How many times does 123 go into \num{1083}? Again,
    eight times. Subtract the product to get the remainder of 99.

    \endgroup
  \end{minipage}
\end{mdframed}
\clearpage

\section{Level 7 - The Super Short Short-Cut}
\label{sec:super-short-short-cut}
When the divisor is small (one or at most two digits), an additional
shortcut can be used. Instead of writing out the product and then
subtracting, the student holds the product in his or her head,
performs the subtraction, and writes the remaining element as a
subscript to the lower right of the dividend digit.

\begin{mdframed}[style=frexample]
  \noindent \textbf{Example 7.1} \quad Solve $\num{632107648} ÷ 5$ with the
  super short cut

  The example below can be thought of as “6 minus 1-times-5 is 1, so
  I’ll write a 1 between the 6 and 3”. In the second step, we use the
  little 1 along with the three, and think “13 minus 2-times-5 is 3,
  so I’ll write a 3 to the lower right of the 3 already there”. The
  third step uses the little three just written like “32 minus
  6-times-5 is 2, so I’ll write a little 2 next to the big 2.” The
  process continues.

  % fig_super-short-cut
  \begin{center}
    \includegraphics[scale=1.25]{graphics/fig_super-short-cut}
  \end{center}
\end{mdframed}



\end{document}