#!/bin/bash
echo "Extraction typefaces"
(cd typefaces &&
     unzip qbk2.004otf.zip &&
     unzip texgyrebonum-math.zip)

echo "Building division (twice)"
(cd math/fractions1 &&
     lualatex division.tex &&
     lualatex division.tex)

echo "Building fractions1 (twice)"
(cd math/fractions1 &&
     lualatex fractions1.tex &&
     lualatex fractions1.tex)

echo "Building fractions2 (twice)"
(cd math/fractions2 &&
     lualatex fractions2.tex &&
     lualatex fractions2.tex)
